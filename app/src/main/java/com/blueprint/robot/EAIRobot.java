package com.blueprint.robot;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.eaibot.konyun.eaibotdemo.JniEAIBot;
import com.yist.eailibrary.bean.bot.Battery;
import com.yist.eailibrary.bean.bot.BotStatus;
import com.yist.eailibrary.constants.HandlerCode;
import com.yist.eailibrary.utils.DataTransUtils;
import com.yist.eailibrary.utils.JniEAIBotUtil;

public class EAIRobot {
    private static final String TAG = "EAI-ROBOT";
    public static JniEAIBotUtil jniEAIBotUtil;

    public static final Handler data_handler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            switch (msg.what) {
                case JniEAIBot.ON_EAI_STAT_INFO:
                    //被加到addDataHandler后该函数会定时执行，从而可以随时获取机器人的一些状态信息
                    BotStatus botStatus = DataTransUtils.getBotStatus(msg.getData().getByteArray("data"));
                    DataTransUtils.getBotPose(msg.getData().getByteArray("data"));
                    break;
                case JniEAIBot.ON_EAI_BATTERY:
                    Battery battery = DataTransUtils.getBattery(msg.getData().getByteArray("data"));
                    break;

                default:
                    break;
            }
            return false;
        }
    });
}
