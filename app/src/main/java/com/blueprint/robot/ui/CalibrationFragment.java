package com.blueprint.robot.ui;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.blueprint.robot.EAIRobot;
import com.blueprint.robot.MainActivity;
import com.blueprint.robot.R;
import com.blueprint.robot.ui.carousel.CarouselFragment;
import com.yist.eailibrary.constants.HandlerCode;
import com.yist.eailibrary.constants.ResponseCode;
import com.yist.eailibrary.constants.RosBotCode;
import com.yist.eailibrary.layer.GoalView;
import com.yist.eailibrary.layer.LayerGroup;
import com.yist.eailibrary.utils.JniEAIBotUtil;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CalibrationFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CalibrationFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private static LayerGroup layerGroup;
    //用于在屏幕上提示用户程序状态
    private Toast toast;
    private View view;
    private Button calibrationButton;
    Intent intent;

    public CalibrationFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CalibrationFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CalibrationFragment newInstance(String param1, String param2) {
        CalibrationFragment fragment = new CalibrationFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        //戴坤灵 添加返回跳转部分
        intent = getActivity().getIntent();
        if(intent!=null) {
            String str1 = intent.getStringExtra("come back");
            if (str1!= null) {
                Log.d("string value",str1);
                if(str1.equals("true")) {
                    Log.d("come back","success");
                    NavController controller = Navigation.findNavController(getActivity(),R.id.fragment);
                    controller.navigate(R.id.action_calibrationFragment_to_functionSelectionFragment);
                }
            }
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_calibration, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        layerGroup = getView().findViewById(R.id.layerGroup);

        Button button = getView().findViewById(R.id.toFuncitonSelectionButton);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavController controller = Navigation.findNavController(view);
                controller.navigate(R.id.action_calibrationFragment_to_functionSelectionFragment);

            }
        });
        calibrationButton = getView().findViewById(R.id.calibrationButton);
        view = getView();
        initRobotAbility();
    }

    private void initLayerGroup(){
        calibrationButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                layerGroup.setTouchMode(1);
            }
        });
        //初始化layerGroup并绑定监听事件
        layerGroup.initLayer(this.getContext(), EAIRobot.jniEAIBotUtil);
        // layerGroup中有一个int型的值touchMode来标志触摸该控件后需要做什么操作，该值可通过其他的按键动态设定
        // 设定完成后触摸控件，触发回调函数时，可以在相应的mode下插入一些操作如提示状态信息，切换到其他mode
        layerGroup.setOnLayerTouchListener(new GoalView.OnLayerTouchListener() {
            @Override
            public void onTouch(int touchMode) {
                switch (touchMode) {
                    case 1:
                        toastLast("正在校准位置");//输出提示信息
                        layerGroup.setTouchMode(0);//1 是校正位置，只用一次即可，后面调整到0为正常状态
                        break;
                    default:
                        break;
                }
            }
        });
        layerGroup.setBotPoseInitListener(new GoalView.BotPoseInitListener() {
            @Override
            public void onBotPoseInit(int result) {
                if (result == 0) {
                    toastLast("正在校准位置");
                } else {
                    if (result == RosBotCode.ROSBOT_ERR_HAD_CONTROLLER)
                        toastLast("您不是主控用户");
                    else if (result == ResponseCode.DISCONNECT)
                        toastLast("请检查网络错误");
                    else
                        toastLast("校准机器人位置失败，请查看" + ": " + result + "," + "来判断其确切含义");
                }
            }
        });
        // 获取名为fisrt的地图，并设置触摸layerGroup的touch mode 为矫正位置的模式
        EAIRobot.jniEAIBotUtil.selectMapByName("win521", eaiRobotHandler, HandlerCode.SELECT_MAP_BY_NAME);
        layerGroup.setTouchMode(1);
    }

    // 初始化机器人控制能力
    private void initRobotAbility() {
        //初始化 jniEAIBotUtil 对象
        EAIRobot.jniEAIBotUtil = new JniEAIBotUtil(this.getActivity().getBaseContext());
        //初始化SDK，该函数为异步执行，在handle返回前所有对jniEAIBotUtil的操作都会导致程序崩溃
        EAIRobot.jniEAIBotUtil.initEAISDKROS("192.168.31.200", 500, 0xFFC1E5FF, Color.WHITE, Color.BLACK, Color.YELLOW, eaiRobotHandler, HandlerCode.INIT_EAISDK);
        // 添加该handler 该handler会定时执行，可以用来获取ros机器人的一些状态信息
        EAIRobot.jniEAIBotUtil.addDataHandler(EAIRobot.data_handler);
    }

    private final Handler eaiRobotHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            Bundle bundle;
            int result;
            int extend;
            String mapName;
            switch (msg.what) {
                case HandlerCode.EAI_LOGIN:
                    bundle = msg.getData();
                    String name = bundle.getString("name");
                    String password = bundle.getString("password");

                    toastLast("登陆成功");
                    initLayerGroup();
                    break;
                case HandlerCode.INIT_EAISDK:
                    toastLast("初始化SDK");
                    EAIRobot.jniEAIBotUtil.eaiLogin("admin", "admin", eaiRobotHandler, HandlerCode.EAI_LOGIN);
                    break;
                case HandlerCode.SELECT_MAP_BY_NAME:
                    bundle = msg.getData();
                    result = bundle.getInt("result");
                    mapName = bundle.getString("mapName");
                    if (result == 0) {
                        toastLast("地图切换成功");
                        layerGroup.clearVirtualWall();
                        layerGroup.clearAllGoals();
                        EAIRobot.jniEAIBotUtil.getMapByName(0, mapName, eaiRobotHandler, HandlerCode.GET_MAP_BY_NAME);
                        // type 要修改的速度类型 0 导航速度, 1 跑路径速度
                        EAIRobot.jniEAIBotUtil.speedControl(0, 2, eaiRobotHandler, HandlerCode.SPEED_CONTROL);
                        EAIRobot.jniEAIBotUtil.speedControl(1, 27, eaiRobotHandler, HandlerCode.SPEED_CONTROL);
                    } else
                        toastLast("获取地图失败");
                    break;
                default:
                    break;
            }
            return false;
        }
    });


    //用于在屏幕上显示提示信息，有多条信息要显示只显示最后一条
    private void toastLast(String message) {
        if (message != null) {
            if (toast != null) {
                toast.cancel();
            }
            toast = Toast.makeText(this.getActivity(), message, Toast.LENGTH_SHORT);
            toast.show();
        }
    }
}