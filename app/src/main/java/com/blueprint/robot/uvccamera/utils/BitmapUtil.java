package com.blueprint.robot.uvccamera.utils;

import android.content.ContentValues;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static com.blueprint.robot.uvccamera.utils.FileOperationUtil.decodeSampleBitmapFormFile;
import static com.blueprint.robot.uvccamera.utils.FileOperationUtil.findFileList;


public class BitmapUtil {
    private static final String TAG = BitmapUtil.class.getSimpleName();

    static int mFilePathNum = 0;

    //根据路径获取图片，返回一个可传入适配器的列表
    public static List<Map<String, Bitmap>> getData(String filepath, Context context) throws IOException {
        //0. 声明变量
        List<Map<String, Bitmap>> list;
        list = new ArrayList<>();
        File file = new File(filepath);
        Map map;

        //1. 得到所有的文件路径，放在一个list中
        Bitmap bm;
        String imageMessageInfo = null;
        List<String> imagePathString = new ArrayList<>();
        findFileList(file,imagePathString);

        //2. 加载为bitmap图片

        Iterator iterator = imagePathString.iterator();
        while (iterator.hasNext()){
            String string = (String)iterator.next();

            bm= decodeSampleBitmapFormFile(string,100,100);

            //saveBitmapImage(bm,context);

            map = new HashMap<String, Bitmap>();

            map.put("image",bm);
            list.add(map);
        }
        return list;
    }

    /**
     * @人脸检测可以用到
     * 用于保存位图到指定的路径
     * @param source
     * @param context
     * @throws IOException
     */
    public static void saveBitmapImage(Bitmap source, Context context) throws IOException {
        //只要是Context对象都行
        // We should store image data earlier than insert it to ContentProvider,
        // otherwise we may not be able to generate thumbnail in time.
        String directory = Environment.getExternalStorageDirectory().getCanonicalPath()+"/DCIM/";//存储路径
        String filename = "123_" + mFilePathNum + ".jpg";//名字，随便你定义
        mFilePathNum ++;

        OutputStream outputStream = null;
        String filePath = directory + "/" + filename;
        try {
            File dir = new File(directory);
            if (!dir.exists()) dir.mkdirs();
            File file = new File(directory, filename);

            outputStream = new FileOutputStream(file);
            source.compress(Bitmap.CompressFormat.JPEG, 75, outputStream);

        } catch (FileNotFoundException ex) {
            Log.v(TAG, "yunhen FileNotFoundException :" + ex);
            return ;
        } finally {
            if (outputStream!= null){
                try {
                    outputStream.close();
                } catch (Throwable t) {
                    // do nothing
                }
            }
        }

        // Read back the compressed file size.
        long size = new File(directory, filename).length();

        ContentValues values = new ContentValues(9);
        // That filename is what will be handed to Gmail when a user shares a
        // photo. Gmail gets the name of the picture attachment from the
        // "DISPLAY_NAME" field.

        values.put(MediaStore.Images.ImageColumns.DATA, filePath);
        values.put(MediaStore.Images.ImageColumns.TITLE, filename);

        values.put(MediaStore.Images.Media.DISPLAY_NAME, filename);
        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");
        values.put(MediaStore.Images.Media.DATA, filePath);
        values.put(MediaStore.Images.Media.SIZE, size);

        context.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        Log.e("yunhen", "yunhen SaveBitmapImage filePath = " + filePath);
    }
}
