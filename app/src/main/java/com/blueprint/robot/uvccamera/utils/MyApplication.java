package com.blueprint.robot.uvccamera.utils;


import android.app.Application;

public class MyApplication extends Application {

    private static com.blueprint.robot.uvccamera.utils.MyApplication application;

    @Override
    public void onCreate() {
        super.onCreate();
        application = this;
    }

    public static com.blueprint.robot.uvccamera.utils.MyApplication getInstance() {
        return application;
    }
}

