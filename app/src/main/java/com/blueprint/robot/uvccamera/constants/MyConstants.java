package com.blueprint.robot.uvccamera.constants;

public class MyConstants {
    //相机状态
    public enum UsbCamState {
        IDLE, RECORDING, WAIT_SURE_VIDEO, WAIT_SURE_PIC, TAKING_PIC
    }
    //相机连接状态
    public enum UsbDeviceState {
        ATTACH,DETTACH,CONNECT,DISCONNECT
    }
    //默认的宽度和高度
    public static int DEFAULT_USB_CAM_WIDTH = 1280;
    public static int DEFAULT_USB_CAM_HEIGHT = 720;

    public static int FORMAT_YUV = 0x01;

    public static final int showWidth = 500;
    public static final int showHeight = 333;
}
