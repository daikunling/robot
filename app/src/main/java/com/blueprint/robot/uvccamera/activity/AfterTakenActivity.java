package com.blueprint.robot.uvccamera.activity;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.blueprint.robot.MainActivity;
import com.blueprint.robot.R;
import com.blueprint.robot.uvccamera.constants.MyConstants;
import com.blueprint.robot.uvccamera.utils.DeleteUtil;
import com.blueprint.robot.uvccamera.utils.FileOperationUtil;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class AfterTakenActivity extends AppCompatActivity {
    //获取路径信息
    Intent intent;
    String picturePath;
    ImageView imageView;
    Bitmap bitmap;
    Button mdeletePic;
    Button mreturnBack;
    Button mtakeAgain;
    Button menterWall;
    DeleteUtil deleteUtil = new DeleteUtil();

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_after_taken);//设置界面

        intent = getIntent();
        picturePath = intent.getStringExtra("picturePath");
        init();
        //显示图片
        if(picturePath!=null) {
            bitmap = FileOperationUtil.decodeSampleBitmapFormFile(picturePath, MyConstants.DEFAULT_USB_CAM_WIDTH, MyConstants.DEFAULT_USB_CAM_HEIGHT);
        }

        if (bitmap == null) {
            //错误提示
            new AlertDialog.Builder(this)
                    .setTitle("标题")
                    .setMessage("加载照片失败了")
                    .setPositiveButton("确定", null)
                    .show();
        } else {

            imageView.setImageBitmap(bitmap);
        }


    }



    private void init() {
        imageView= findViewById(R.id.picture);
        mdeletePic = findViewById(R.id.delete);
        mreturnBack = findViewById(R.id.return_back);
        mtakeAgain = findViewById(R.id.take_again);
        menterWall = findViewById(R.id.enter_wall);

        //returnBack
        mreturnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //跳转
                Intent intent = new Intent(AfterTakenActivity.this, MainActivity.class);
                intent.putExtra("come back","true");
                startActivity(intent);
            }
        });
        //takeAgain
        mtakeAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //跳转
                Intent intent = new Intent(AfterTakenActivity.this, TakePhotoActivity.class);
                startActivity(intent);
            }
        });

        //deletePhoto
        mdeletePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FileInputStream fileInputStream;
                try {
                    fileInputStream=new FileInputStream(picturePath);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }

                if (deleteUtil.delete(picturePath)) {
                    Toast.makeText(AfterTakenActivity.this, "删除成功", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(AfterTakenActivity.this, "删除失败", Toast.LENGTH_LONG).show();
                }
            }

        });

        //enter picWall
        menterWall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //跳转
                Intent intent = new Intent(AfterTakenActivity.this, WallActivity.class);
                startActivity(intent);
            }
        });
    }




}
