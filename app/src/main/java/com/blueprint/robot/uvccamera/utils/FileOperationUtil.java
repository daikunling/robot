package com.blueprint.robot.uvccamera.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class FileOperationUtil {


    /**
     *
     * @param options 选项
     * @param reqWidth 目标宽度
     * @param reqHeight 目标高度
     * @return
     */
    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight){
        //源图片的高度和宽度
        final int height = options.outHeight;
        final int width = options.outHeight;
        int inSampleSize = 1;
        if(height>reqHeight||width>reqWidth){
            //计算出实际宽高和目标宽高的比率
            final int heightRatio = Math.round((float)height/(float)reqHeight);
            final int widthRatio =  Math.round((float)width/(float)reqWidth);
            //选择最小的比率作为inSampleSize的值，保证最终图片的宽和高大于需求
            inSampleSize = heightRatio< widthRatio? heightRatio: widthRatio;
        }
        return inSampleSize;
    }

    public static Bitmap decodeSampleBitmapFormFile(String filepath, int reqWidth, int reqHeight){
        //第一次解析 为了获取图片大小
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filepath,options);
        //调用上边定义的方法计算inSampleSize的值
        options.inSampleSize = calculateInSampleSize(options,reqWidth,reqHeight);
        //使用获取到的inSampleSize再次解析图片
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(filepath,options);
    }


    /**
     * 读取目录下的所有文件
     *
     * @param dir
     *            目录
     * @param fileNames
     *            保存文件名的集合
     * @return
     */
    public static void findFileList(File dir, List<String> fileNames) {
        if (!dir.exists() || !dir.isDirectory()) {// 判断是否存在目录
            return;
        }
        File[] files = dir.listFiles();// 读取目录下的所有目录文件信息
        if(files == null)
            return;

        for (int i = 0; i < files.length; i++) {// 循环，添加文件名或回调自身

            if (checkIsImageFile(files[i].getPath())) {// 如果文件
                fileNames.add(files[i].getPath());// 添加文件全路径名
            } else if(files[i].isDirectory()){// 如果是目录
                findFileList(files[i], fileNames);// 回调自身继续查询
            }
        }
    }

    /**
     * @Deprecated please use findFileList instead
     * @param path 相册文件夹路径 “/sdcard/DCIM/UsbCamera/Image”
     * @return 文件夹中所有照片的路径列表
     */
    @Deprecated
    public static List<String> getFilesAllName(String path){
        //传入指定文件夹的路径
        File file = new File(path);
        if(file==null)
            return null;

        File[] files = file.listFiles();

        if(files == null)
            return null;

        List<String> imagePaths = new ArrayList<>();
        for(int i=0;i<files.length;i++){
            if(checkIsImageFile(files[i].getPath())){
                imagePaths.add(files[i].getPath());
            }
        }
        return imagePaths;
    }

    /**
     * 这是一个二级函数，被findFileList调用
     * @param fName 文件名称
     * @return 此文件是不是图片文件
     */
    public static boolean checkIsImageFile(String fName){
        boolean isImageFile = false;
        //获取拓展名
        String fileEnd = fName.substring(fName.lastIndexOf(".")+1,
                fName.length()).toLowerCase();
        if(fileEnd.equals("jpg")||fileEnd.equals("png") || fileEnd.equals("gif")
                    || fileEnd.equals("jpeg")|| fileEnd.equals("bmp")){
            isImageFile = true;
        }else {
            isImageFile = false;
        }
        return isImageFile;
    }

    /**
     * 测试使用 保存图片路径
     * 保存所有图片的路径信息到DCIM下一个fff.txt文件中。
     */
    public static void saveFile(Context context) throws IOException {
        String filePath="";
        File file;
        Context context1;
        context1 = context;
        //新建一个文件
        file = new File(context1.getExternalFilesDir(null).getCanonicalPath()+"/DCIM","fff.txt");
        //file = new File(Environment.getExternalStorageDirectory()+"/DCIM", "fff.txt");

        List<String> str1 = getFilesAllName(Environment.getExternalStorageDirectory().getCanonicalPath()+"/DCIM/Camera");

        Iterator iterator1 = str1.iterator();
        while (iterator1.hasNext()){
            filePath += (String)iterator1.next() +"\n";
        }
        //========
        FileOutputStream outStream = null;
        try {
            outStream = new FileOutputStream(file);
            outStream.write(filePath.getBytes());
            outStream.flush();
            System.out.println("successfully write down");

        } catch (FileNotFoundException fileNotFoundException) {
            fileNotFoundException.printStackTrace();
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }finally {
            if(outStream!= null){
                try {
                    outStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * 删除单个文件
     *
     * @param fileName
     *            要删除的文件的文件名
     * @return 单个文件删除成功返回true，否则返回false
     */
    public static boolean deleteFile(String fileName) {
        File file = new File(fileName);
        // 如果文件路径所对应的文件存在，并且是一个文件，则直接删除
        if (file.exists() && file.isFile()) {
            if (file.delete()) {
                System.out.println("删除单个文件" + fileName + "成功！");
                return true;
            } else {
                System.out.println("删除单个文件" + fileName + "失败！");
                return false;
            }
        } else {
            System.out.println("删除单个文件失败：" + fileName + "不存在！");
            return false;
        }
    }


    public static String[] convertToStringArray(ArrayList<String> list) {
        String[] covertedArray = new String[list.size()];
        covertedArray = (String[]) list.toArray(covertedArray);
        return covertedArray;
    }




}
