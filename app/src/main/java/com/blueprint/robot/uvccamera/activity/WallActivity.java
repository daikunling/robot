package com.blueprint.robot.uvccamera.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.SimpleAdapter;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;


import com.blueprint.robot.MainActivity;
import com.blueprint.robot.R;
import com.blueprint.robot.uvccamera.module.PhotoWallAdapter;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.blueprint.robot.uvccamera.utils.BitmapUtil.getData;
import static com.blueprint.robot.uvccamera.utils.FileOperationUtil.convertToStringArray;
import static com.blueprint.robot.uvccamera.utils.FileOperationUtil.findFileList;
import static com.blueprint.robot.uvccamera.utils.FileOperationUtil.saveFile;


public class WallActivity extends AppCompatActivity {

    private static String TAG = WallActivity.class.getSimpleName();
    public static final boolean debug = false;

    //声明组件
    GridView gview;
    Button button;
    List<Map<String, Bitmap>> data_list = null;
    List<String> imagePathString;
    //老版本使用SimpleAdapter
    SimpleAdapter sim_adapter;
    //新的PhotoWallAdapter
    PhotoWallAdapter ptw_adapter;

     @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.photo_wall);

        //绑定组件
        gview = (GridView) findViewById(R.id.gview);
        button = findViewById(R.id.btn_return1);

         Log.d("filefile", "" );

         button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(WallActivity.this, MainActivity.class);
                intent.putExtra("come back","true");
                startActivity(intent);
            }
        });
        //打印所有照片路径的信息
//        if(debug) {
//            try {
//               // saveFile();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }

        //获取一组List<Map<String,Bitmap>> 类型的数据
         //新版本需要的数据
         imagePathString = new ArrayList<>();

         try {
             Log.d("filefile", Environment.getExternalStorageDirectory().getCanonicalPath());
         } catch (IOException e) {
             e.printStackTrace();
         }

         //老AD版本
         File file = null;
         try {
             //file = new File(String.valueOf(Uri.parse("content://com.android.externalstorage.documents/document/primary:"+ "%2fDCIM")));
             file = new File(Environment.getExternalStorageDirectory().getCanonicalPath()+"/DCIM");
         } catch (IOException e) {
             e.printStackTrace();
         }
         findFileList(file,imagePathString);

         //简易版本的数据
         //data_list = getData(Environment.getExternalStorageDirectory().getCanonicalPath()+"/DCIM",this);

         if(imagePathString!= null) {
            String[] pathList = convertToStringArray((ArrayList<String>) imagePathString);
            ptw_adapter = new PhotoWallAdapter(this, 0, pathList, gview, (ArrayList<String>) imagePathString);
            gview.setAdapter(ptw_adapter);
        }

        //配置适配器
        if(false) {
            //新建适配器
            String[] from = {"image"};
            int[] to = {R.id.image};
            sim_adapter = new SimpleAdapter(WallActivity.this, data_list, R.layout.cell_photo_wall, from, to);
            sim_adapter.setViewBinder(new SimpleAdapter.ViewBinder() {
                @Override
                public boolean setViewValue(View view, Object o, String s) {
                    if(view instanceof ImageView && o instanceof Bitmap){
                        ImageView iv=(ImageView)view;
                        iv.setImageBitmap((Bitmap) o);
                        return true;
                    }else{
                        return false;
                    }
                }
            });
            //配置适配器
            gview.setAdapter(sim_adapter);

        }
//            //错误提示
//            new  AlertDialog.Builder(this)
//                    .setTitle("标题" )
//                    .setMessage("失败了" )
//                    .setPositiveButton("确定" ,  null )
//                    .show();
//

    }
}
