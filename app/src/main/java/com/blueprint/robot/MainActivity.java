package com.blueprint.robot;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.hardware.usb.UsbManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.MotionEvent;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.blueprint.robot.data.ViewModel.ScenicSpotViewModel;
import com.blueprint.robot.data.entity.ScenicSpot;
import com.blueprint.robot.ui.SidebarFragment;
import com.iflytek.cloud.SpeechConstant;
import com.iflytek.cloud.SpeechUtility;
import com.yist.eailibrary.constants.HandlerCode;
import com.yist.eailibrary.utils.JniEAIBotUtil;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import cn.wch.ch34xuartdriver.CH34xUARTDriver;

public class MainActivity<datetime> extends AppCompatActivity {
    private static final String TAG = "hefeng now";
    private static final int TIME_VALUE = 1000 * 60 * 100;//无操作跳转时间，单位毫秒
    private static final int CAROUSEL_MODE = 0x102;
    private boolean isCarousel = false;
    //用于在屏幕上提示用户程序状态
    private Toast toast;

    @Override
    protected void onResume() {
        super.onResume();
        mHandler.sendEmptyMessageDelayed(CAROUSEL_MODE, TIME_VALUE);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //  设置屏幕常亮
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        //状态栏透明
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
        }


        ScenicSpotViewModel scenicSpotViewModel = new ViewModelProvider(this).get(ScenicSpotViewModel.class);//通过ScenicSpotViewModel对象获取所有景点信息
        List<ScenicSpot> scenicSpotList = scenicSpotViewModel.getScenicSpotList();
        if (scenicSpotList.isEmpty()) {
            for (int i = 0; i < 7; i++) {
                ScenicSpot scenicSpot;
                List<Integer> picList = new ArrayList<>();
                switch (i) {

                    case 0:
                        scenicSpot = new ScenicSpot("欢迎来到大学生创新创业实践成果展", "", "", 3f, "欢迎来到北京邮电大学第十二届大学生创新创业实践成果展暨创新创业论坛" ,"" , "st");
                        picList.add(R.drawable.logo1);
                        break;

                    case 1:
                        scenicSpot = new ScenicSpot("欢迎来到大学生创新创业实践成果展", "", "", 3f, "欢迎来到北京邮电大学第十二届大学生创新创业实践成果展暨创新创业论坛,大创展自2009年举办至今已经连续成功举办十一届，累计参观人次超过10万，成为集项目展示、合作交流、研讨提升、资源对接的大平台，成为我校覆盖面最广、影响力最大的品牌活动。可点击屏幕下方按钮继续参观。\n" ,"" , "st");
                        picList.add(R.drawable.logo1);
                        break;

                    case 2:
                        scenicSpot = new ScenicSpot("3-6号雏雁优秀项目", "", "", 3f, "你们左侧是四个雏燕计划项目。雏雁计划是为大创计划做孵化和培育的，主要侧重创意的激发，大一的学生10月份入学后参加雏雁计划，第二年4月份以雏雁创意大赛结题，本次展出的都是雏雁的优秀项目。 优秀的雏雁项目可以直接转成大创项目继续打磨。请大家自行浏览，可点击屏幕下方按钮继续参观。", "", "cy");
                        picList.add(R.drawable.chuyan);
                        break;


                    case 3:
                        scenicSpot = new ScenicSpot("7号项目：一种新型温度和pH双功能纳米传感器", "", "", 3f, "我旁边的7号项目是一种新型温度和pH双功能纳米传感器。纳米颗粒具有高的温度和pH敏感性，基于比率荧光探测方法可检测不同环境中的温度和pH变化。可点击屏幕下方按钮继续参观。", "WIN1", "dc1");
                        picList.add(R.drawable.nami);
                        break;


                    case 4:
                        scenicSpot = new ScenicSpot("8号项目：基于磁耦合谐振的的曲面线圈空间无线输电装置", "", "", 3f, "我旁边的8号项目：基于磁耦合谐振的的曲面线圈空间无线输电装置，利用磁耦合谐振无线输电的原理制造一种能够像WIFI为电子设备提供网络一样的无线输电装置，能够为一定空间内的多个用电器同时进行无线供电。可点击屏幕下方按钮继续参观。", "WIN2", "dc2");
                        picList.add(R.drawable.ciouhe);
                        break;


                    case 5:
                        scenicSpot = new ScenicSpot("9号项目：毛细管光学实验仪的研制", "", "", 3f, "我身后的9号项目：毛细管光学实验仪中的毛细管是中空的透明玻璃管，内部可充填透明液体，激光入射到毛细管上，会形成非常丰富的衍射条纹。本试验仪就是采集这些衍射条纹，然后获取条纹光强与角度的关系曲线。可点击屏幕下方按钮继续参观。", "WIN3", "dc3");
                        picList.add(R.drawable.maoxi);
                        break;


                    case 6:
                        scenicSpot = new ScenicSpot("10号项目：多自由度柔性导管机器人", "", "", 3f, "我身后的10号项目：多自由度柔性导管机器人将采用未曾在此领域应用过的扭绳驱动方式作为动力系统，并且将采用蓝牙进行数据传输，实现非直接接触的操作。今后可以用到医疗手术中作为辅助手段。这就是我为大家带来的全部讲解，谢谢。", "WIN4", "dc4");
                        picList.add(R.drawable.daoguan);
                        break;
                    default:
                        scenicSpot = new ScenicSpot();
                }
                scenicSpot.setScenicPicUrlList(picList);
                scenicSpotViewModel.insertScenicSpot(scenicSpot);
            }
        }
        setContentView(R.layout.activity_main2);
        initVoiceAbility();
        requestPermissions();
    }

    // 初始化语音能力，同时设置语音板子 CH34xUART 以及 讯飞端口
    private void initVoiceAbility() {
        CH34xUARTDriverHelper.driver = new CH34xUARTDriver(
                (UsbManager) getSystemService(Context.USB_SERVICE), this,
                "com.bupt.robot.USB_PERMISSION");
        if (!CH34xUARTDriverHelper.isOpen) {
            int retVal = CH34xUARTDriverHelper.driver.ResumeUsbList();
            if (retVal == -1)// ResumeUsbList方法用于枚举CH34X设备以及打开相关设备
            {
                toastLast("打开设备失败!");
                CH34xUARTDriverHelper.driver.CloseDevice();
            } else if (retVal == 0) {
                if (!CH34xUARTDriverHelper.driver.UartInit()) {//对串口设备进行初始化操作
                    toastLast("设备初始化失败!打开设备失败");
                    return;
                }
                toastLast("打开设备成功!");
                CH34xUARTDriverHelper.isOpen = true;
                CH34xUARTDriverHelper.driver.SetConfig(115200, (byte) 8, (byte) 0, (byte) 0, (byte) 0);
            } else {
                toastLast("未授权限");
            }
        }
        //初始化讯飞SDK，并且请求对应的语音操作权限
        SpeechUtility.createUtility(this, SpeechConstant.APPID + "=" + getString(R.string.app_id));
    }

    private void requestPermissions() {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                int permission = ActivityCompat.checkSelfPermission(this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE);
                if (permission != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(this, new String[]{
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.LOCATION_HARDWARE, Manifest.permission.READ_PHONE_STATE,
                            Manifest.permission.WRITE_SETTINGS, Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.RECORD_AUDIO, Manifest.permission.READ_CONTACTS}, 0x0010);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private static class MainHandler extends Handler {
        private final WeakReference<MainActivity> mainActivityWeakReference;

        private MainHandler(MainActivity mainActivity) {
            super(Looper.getMainLooper());
            mainActivityWeakReference = new WeakReference<MainActivity>(mainActivity);
        }

        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            if (SidebarFragment.inWakeUp)
                return;
            MainActivity mainActivity = mainActivityWeakReference.get();
            if (mainActivity != null) {
                switch (msg.what) {
                    case CAROUSEL_MODE://跳转至巡航轮播
                        mainActivity.setCarousel(true);
                        NavController navController = Navigation
                                .findNavController(mainActivity, R.id.fragment);
                        navController.navigate(R.id.carouselFragment);
                        break;
                    default:
                        break;
                }
            }
        }
    }

    private MainHandler mHandler = new MainHandler(this);

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        Log.d(TAG, "dispatchTouchEvent: click" + isCarousel);
        if (!isCarousel) {//当前不在巡航轮播界面时
            if (mHandler.hasMessages(CAROUSEL_MODE)) {
                mHandler.removeMessages(CAROUSEL_MODE);
            }
            Log.d(TAG, "dispatchTouchEvent: send");
            mHandler.sendMessageDelayed(mHandler.obtainMessage(CAROUSEL_MODE), TIME_VALUE);
        }
        return super.dispatchTouchEvent(ev);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mHandler.hasMessages(CAROUSEL_MODE)) {
            mHandler.removeMessages(CAROUSEL_MODE);
        }
    }
    private final Handler eaiRobotHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            int extend;
            switch (msg.what) {
                case HandlerCode.CLOSE_EAISDK:
                    extend = msg.getData().getInt("extend");
                    if (extend == 1)
                        EAIRobot.jniEAIBotUtil.initEAISDKROS("192.168.31.200", 500, 0xFF9DC4DF, Color.WHITE, Color.BLACK, Color.YELLOW, eaiRobotHandler, HandlerCode.INIT_EAISDK);
                    else if (extend == 0)
                        MainActivity.this.finish();
                    break;
                default:
                    break;
            }
            return false;
        }
    });

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mHandler.removeCallbacksAndMessages(null);
        CH34xUARTDriverHelper.driver.CloseDevice();
        CH34xUARTDriverHelper.isOpen = false;
        EAIRobot.jniEAIBotUtil.closeEAISDK(0, eaiRobotHandler, HandlerCode.CLOSE_EAISDK);
    }

    public void setCarousel(boolean carousel) {
        isCarousel = carousel;
    }

    public void reInitTime() {
        mHandler.sendMessageDelayed(mHandler.obtainMessage(CAROUSEL_MODE), TIME_VALUE);
    }

    //用于在屏幕上显示提示信息，有多条信息要显示只显示最后一条
    private void toastLast(String message) {
        if (message != null) {
            if (toast != null) {
                toast.cancel();
            }
            toast = Toast.makeText(this, message, Toast.LENGTH_SHORT);
            toast.show();
        }
    }
}


//package com.blueprint.robot;
//
//import android.Manifest;
//import android.app.AlertDialog;
//import android.content.Context;
//import android.content.Intent;
//import android.content.pm.PackageManager;
//import android.graphics.Color;
//import android.hardware.usb.UsbManager;
//import android.os.Build;
//import android.os.Bundle;
//import android.os.Handler;
//import android.os.Looper;
//import android.os.Message;
//import android.util.Log;
//import android.view.MotionEvent;
//import android.view.WindowManager;
//import android.widget.Toast;
//
//import androidx.annotation.NonNull;
//import androidx.appcompat.app.ActionBar;
//import androidx.appcompat.app.AppCompatActivity;
//import androidx.core.app.ActivityCompat;
//import androidx.lifecycle.ViewModelProvider;
//import androidx.navigation.NavController;
//import androidx.navigation.Navigation;
//
//import com.blueprint.robot.data.ViewModel.ScenicSpotViewModel;
//import com.blueprint.robot.data.entity.ScenicSpot;
//import com.blueprint.robot.ui.SidebarFragment;
//import com.iflytek.cloud.SpeechConstant;
//import com.iflytek.cloud.SpeechUtility;
//import com.yist.eailibrary.constants.HandlerCode;
//import com.yist.eailibrary.utils.JniEAIBotUtil;
//
//import java.lang.ref.WeakReference;
//import java.util.ArrayList;
//import java.util.List;
//
//import cn.wch.ch34xuartdriver.CH34xUARTDriver;
//
//public class MainActivity<datetime> extends AppCompatActivity {
//    private static final String TAG = "hefeng now";
//    private static final int TIME_VALUE = 1000 * 60 * 10;//无操作跳转时间，单位毫秒
//    private static final int CAROUSEL_MODE = 0x102;
//    private static final int SELECTION_MODE = 0x105;
//    private boolean isCarousel = false;
//    private boolean isSelection = false;
//    //用于在屏幕上提示用户程序状态
//    private Toast toast;
//
//    //用于跳转
//    private Intent intent;
//    private String rcvExtraString="";
//
//    @Override
//    protected void onResume() {
//        super.onResume();
//        mHandler.sendMessageDelayed(mHandler.obtainMessage(SELECTION_MODE),TIME_VALUE);
//    }
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        //  设置屏幕常亮
//        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
//        ActionBar actionBar = getSupportActionBar();
//        if (actionBar != null) {
//            actionBar.hide();
//        }
//        //状态栏透明
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
//            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
//            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
//        }
//
//
//
//
//        ScenicSpotViewModel scenicSpotViewModel = new ViewModelProvider(this).get(ScenicSpotViewModel.class);//通过ScenicSpotViewModel对象获取所有景点信息
//        List<ScenicSpot> scenicSpotList = scenicSpotViewModel.getScenicSpotList();
//        if (scenicSpotList.isEmpty()) {
//            for (int i = 0; i < 3; i++) {
//                ScenicSpot scenicSpot;
//                List<Integer> picList = new ArrayList<>();
//                switch (i) {
//                    case 0:
//                        scenicSpot = new ScenicSpot("神泉潮井", "每周一到周五上午10点，周六周天上午12点", "每周一到周五晚上10点，周六周天晚上10点", 3f, "核心景观潮井：涨潮时，水从泉眼汩汩而出，\n如银花怒放，似珍珠滚出，间有小鱼舞动，妙趣横生。\n一涨一落持续5—10分钟，最大流量100升/秒以上。\n一年四季每日不定时涨落，多时每日涨潮七、八次，少时二、三次，\n时间不定，无规律可循，周而复始，终年不息。", 10);
//                        picList.add(R.drawable.attractionimage1);
//                        break;
//                    case 1:
//                        scenicSpot = new ScenicSpot("神泉驿站", "每周一到周五上午10点，周六周天上午12点", "每周一到周五晚上10点，周六周天晚上10点", 3f, "神泉驿站主要由潮井湿地公园、葫芦湖、潮井神泉、瀑布等景观组成，\n有亭台水榭、品茗茶室、特色商铺、餐饮等配套设施，集吃喝玩乐于一体。", 10);
//                        picList.add(R.drawable.attractionimage2);
//                        break;
//                    case 2:
//                        scenicSpot = new ScenicSpot("粉黛草观赏区", "每周一到周五上午10点，周六周天上午12点", "每周一到周五晚上10点，周六周天晚上10点", 3f, "每年的8月下旬到11月中旬，是粉黛乱子草盛开的季节，\n花穗呈云雾状，远看如红色云雾，十分梦幻，简直是少女心爆棚的地方。", 10);
//                        picList.add(R.drawable.attractionimage3);
//                        break;
//                    default:
//                        scenicSpot = new ScenicSpot();
//                }
//                scenicSpot.setScenicPicUrlList(picList);
//                scenicSpotViewModel.insertScenicSpot(scenicSpot);
//            }
//        }
//
//        //设置跳转方式
//        intent = getIntent();
//        rcvExtraString = intent.getStringExtra("come back");
//        new  AlertDialog.Builder(this)
//                .setTitle("标题 返回的字符" )
//                .setMessage(rcvExtraString)
//                .setPositiveButton("确定" ,  null )
//                .show();
//        if(rcvExtraString!=null) {
//            // 跳过设置位姿，直接跳转到四大模块部分
//            if (rcvExtraString.equals("true")) {
//                Log.d("return back","返回功能选择界面");
//                setCarousel(false);
//                mHandler.sendMessageDelayed(mHandler.obtainMessage(SELECTION_MODE),TIME_VALUE);
//                setContentView(R.layout.fragment_function_selection);
//                setCarousel(false);
//                setSelection(true);
//                NavController navController_sel = Navigation
//                        .findNavController(this, R.id.fragment);
//                navController_sel.navigate(R.id.action_calibrationFragment_to_functionSelectionFragment);
//            }
//        }else {
//            //设置调整位姿
//            Log.d("return back","进入调整位姿界面");
//            mHandler.sendEmptyMessageDelayed(CAROUSEL_MODE, TIME_VALUE);
//            setContentView(R.layout.activity_main2);
//        }
//
//        initVoiceAbility();
//        requestPermissions();
//    }
//
//    // 初始化语音能力，同时设置语音板子 CH34xUART 以及 讯飞端口
//    private void initVoiceAbility() {
//        CH34xUARTDriverHelper.driver = new CH34xUARTDriver(
//                (UsbManager) getSystemService(Context.USB_SERVICE), this,
//                "com.bupt.robot.USB_PERMISSION");
//        if (!CH34xUARTDriverHelper.isOpen) {
//            int retVal = CH34xUARTDriverHelper.driver.ResumeUsbList();
//            if (retVal == -1)// ResumeUsbList方法用于枚举CH34X设备以及打开相关设备
//            {
//                toastLast("打开设备失败!");
//                CH34xUARTDriverHelper.driver.CloseDevice();
//            } else if (retVal == 0) {
//                if (!CH34xUARTDriverHelper.driver.UartInit()) {//对串口设备进行初始化操作
//                    toastLast("设备初始化失败!打开设备失败");
//                    return;
//                }
//                toastLast("打开设备成功!");
//                CH34xUARTDriverHelper.isOpen = true;
//                CH34xUARTDriverHelper.driver.SetConfig(115200, (byte) 8, (byte) 0, (byte) 0, (byte) 0);
//            } else {
//                toastLast("未授权限");
//            }
//        }
//        //初始化讯飞SDK，并且请求对应的语音操作权限
//        SpeechUtility.createUtility(this, SpeechConstant.APPID + "=" + getString(R.string.app_id));
//    }
//
//    private void requestPermissions() {
//        try {
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                int permission = ActivityCompat.checkSelfPermission(this,
//                        Manifest.permission.WRITE_EXTERNAL_STORAGE);
//                if (permission != PackageManager.PERMISSION_GRANTED) {
//                    ActivityCompat.requestPermissions(this, new String[]{
//                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
//                            Manifest.permission.LOCATION_HARDWARE, Manifest.permission.READ_PHONE_STATE,
//                            Manifest.permission.WRITE_SETTINGS, Manifest.permission.READ_EXTERNAL_STORAGE,
//                            Manifest.permission.RECORD_AUDIO, Manifest.permission.READ_CONTACTS}, 0x0010);
//                }
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//
//    private static class MainHandler extends Handler {
//        private final WeakReference<MainActivity> mainActivityWeakReference;
//
//        private MainHandler(MainActivity mainActivity) {
//            super(Looper.getMainLooper());
//            mainActivityWeakReference = new WeakReference<MainActivity>(mainActivity);
//        }
//
//        @Override
//        public void handleMessage(@NonNull Message msg) {
//            super.handleMessage(msg);
//            if (SidebarFragment.inWakeUp)
//                return;
//            MainActivity mainActivity = mainActivityWeakReference.get();
//            if (mainActivity != null) {
//                switch (msg.what) {
//                    case SELECTION_MODE://跳转至选择界面
//                        mainActivity.setCarousel(false);
//                        mainActivity.setSelection(true);
//                        NavController navController_sel = Navigation
//                                .findNavController(mainActivity, R.id.fragment);
//                        navController_sel.navigate(R.id.functionSelectionFragment);
//                    case CAROUSEL_MODE://跳转至巡航轮播
//                        mainActivity.setCarousel(true);
//                        NavController navController = Navigation
//                                .findNavController(mainActivity, R.id.fragment);
//                        navController.navigate(R.id.carouselFragment);
//                        break;
//                    default:
//                        break;
//                }
//            }
//        }
//    }
//
//    private MainHandler mHandler = new MainHandler(this);
//
//    @Override
//    public boolean dispatchTouchEvent(MotionEvent ev) {
//        Log.d(TAG, "dispatchTouchEvent: click" + isCarousel);
//        if (!isCarousel) {//当前不在巡航轮播界面时
//            if (mHandler.hasMessages(CAROUSEL_MODE)) {
//                mHandler.removeMessages(CAROUSEL_MODE);
//            }
//            Log.d(TAG, "dispatchTouchEvent: send");
//            mHandler.sendMessageDelayed(mHandler.obtainMessage(CAROUSEL_MODE), TIME_VALUE);
//        }
//        return super.dispatchTouchEvent(ev);
//    }
//
//    @Override
//    protected void onPause() {
//        super.onPause();
//        if (mHandler.hasMessages(CAROUSEL_MODE)) {
//            mHandler.removeMessages(CAROUSEL_MODE);
//        }
//    }
//    private final Handler eaiRobotHandler = new Handler(new Handler.Callback() {
//        @Override
//        public boolean handleMessage(Message msg) {
//            int extend;
//            switch (msg.what) {
//                case HandlerCode.CLOSE_EAISDK:
//                    extend = msg.getData().getInt("extend");
//                    if (extend == 1)
//                        EAIRobot.jniEAIBotUtil.initEAISDKROS("192.168.31.200", 500, 0xFF9DC4DF, Color.WHITE, Color.BLACK, Color.YELLOW, eaiRobotHandler, HandlerCode.INIT_EAISDK);
//                    else if (extend == 0)
//                        MainActivity.this.finish();
//                    break;
//                default:
//                    break;
//            }
//            return false;
//        }
//    });
//
//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//        mHandler.removeCallbacksAndMessages(null);
//        CH34xUARTDriverHelper.driver.CloseDevice();
//        CH34xUARTDriverHelper.isOpen = false;
//        EAIRobot.jniEAIBotUtil.closeEAISDK(0, eaiRobotHandler, HandlerCode.CLOSE_EAISDK);
//    }
//
//    public void setCarousel(boolean carousel) {
//        isCarousel = carousel;
//    }
//
//    public void setSelection(boolean selection){
//        isSelection = selection;
//    }
//
//    public void reInitTime() {
//        mHandler.sendMessageDelayed(mHandler.obtainMessage(CAROUSEL_MODE), TIME_VALUE);
//    }
//
//    //用于在屏幕上显示提示信息，有多条信息要显示只显示最后一条
//    private void toastLast(String message) {
//        if (message != null) {
//            if (toast != null) {
//                toast.cancel();
//            }
//            toast = Toast.makeText(this, message, Toast.LENGTH_SHORT);
//            toast.show();
//        }
//    }
//}